module.exports = function (app, models) {
	var message_module = {};
	
	message_module.getAllMessages = function (req, callback) {
		// no login necessary
		models.messages.find({}, function (err, messages) {
			if (err || !messages) return callback({error: 'Sorry. There was an error obtaining the messages.'});
			
			return callback({data: messages});
		});
	};
	
	message_module.saveMessage = function (req, callback) {
		checkUser(req, models, function(user) {
			if (user.error) return callback({error: user.error});
			
			var message = new models.messages({
				email: user.email,
				name: user.name,
				timestamp: (new Date()).getTime(),
				text: req.body.text
			});
			
			message.save(function (err, newMessage) {
				if (err) return callback({error: 'Sorry. There was a problem saving your message.'});
				
				return callback({message: newMessage});
			});
		});
	};
	
	return message_module;
};

function checkUser(req, models, callback) {
	if (!req.session.email) return callback({error: 'Please login to see your data'});
	var query = {email: req.session.email};
	models.users.findOne(query, function (err, user) {
		if (!user || err) return callback({error: 'Please login to see your data'});
		
		return callback(user);
	});
}