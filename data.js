module.exports = function (app, models) {
	var data_module = {};
	
	data_module.getAllData = function (req, callback) {
		checkUser(req, models, function(user) {
			if (user.error) return callback({error: user.error});
			
			return callback({data: user.weeks});
		});
	};
	
	data_module.getPointsData = function(req, callback) {
		checkUser(req, models, function(user) {
			if (user.error) return callback({error: user.error});
			
			var total_points = getTotalPoints(user);
			
			return callback({
				data: {
					total: total_points
				}
			});
		});
	};
	
	data_module.getRankingsData = function (req, callback) {
		// no login necessary
		models.users.find({}, function (err, users) {
			var usersList = [];
			users.forEach( function (user) {
				var userObject = {};
				userObject.email = user.email;
				userObject.name = user.name;
				userObject.points = getTotalPoints(user);
				usersList.push(userObject);
			});
			
			var sortedUsers = [];
			var length = usersList.length;
			for (var i = 0; i < length; i++) {
				var highest = 0;
				var highest_index = 0;
				for (var j = 0; j < usersList.length; j++) {
					if (usersList[j].points > highest) {
						highest = usersList[j].points;
						highest_index = j;
					}
				}
				usersList[highest_index].rank = i + 1;
				sortedUsers.push(usersList[highest_index]);
				usersList.splice(highest_index, 1);
			}
			
			return callback({data: sortedUsers});
		});
	};
	
	data_module.getDayData = function (req, callback) {
		checkUser(req, models, function(user) {
			if (user.error) return callback({error: user.error});
			
			var week = req.body.week;
			var date = req.body.date;
			if (typeof week === 'undefined' || !date) {
				return callback({error: 'Invalid request'});
			}
			
			if (typeof user.weeks !== 'undefined' && user.weeks[week]) {
				return callback({data: user.weeks[week][date]});
			} else {
				return callback();
			}
		});
	};
	
	data_module.saveDayData = function (req, callback) {
		checkUser(req, models, function (user) {
			if (user.error) return callback({error: user.error});
			
			var week = req.body.week;
			var date = req.body.date;
			var new_data = req.body.data;
			if (!new_data) {
				new_data = {};
			}
			if (typeof week === 'undefined' || !date) {
				return callback({error: 'Invalid request'});
			}
			
			if (!user.weeks) {
				user.weeks = new Array(rules.weeks);
			}
			if (!user.weeks[week]) {
				user.weeks[week] = {};
			}
			user.weeks[week][date] = new_data;
			user.markModified('weeks');
			
			user.save(function (err) {
				if (err) return callback({error: 'There was an error saving your data. Sorry about that.'});
				
				return callback({error: false});
			});
		});
	};
	
	return data_module;
};

function checkUser(req, models, callback) {
	if (!req.session.email) return callback({error: 'Please login to see your data'});
	var query = {email: req.session.email};
	models.users.findOne(query, function (err, user) {
		if (!user || err) return callback({error: 'Please login to see your data'});
		
		return callback(user);
	});
}

function getTotalPoints(user) {
	var total_points = 0;
	for (var week = 0; week < user.weeks.length; week++) {
		if (!user.weeks[week]) {
			continue;
		}
		
		// bonuses
		var bonuses = {};
		for (var bonusGoal in rules.bonuses) {
			bonuses[bonusGoal] = true;
		}
		
		for (var day in user.weeks[week]) {
			if (!user.weeks[week][day]) {
				continue;
			}
			
			var dayObject = user.weeks[week][day];
			
			// bonuses
			for (bonusGoal in bonuses) {
				if (!dayObject[bonusGoal]) {
					bonuses[bonusGoal] = false;
				}
			}
			
			for (var goal in dayObject) {
				if (!dayObject[goal]) {
					continue;
				}
				
				var points = rules.dailyGoals[goal][dayObject[goal]].points;
				if (rules.weeklyDouble[week] === goal) {
					points *= 2;
				}
				total_points += points;
			}
		}
		
		// bonuses
		for (bonusGoal in bonuses) {
			if (bonuses[bonusGoal]) {
				total_points += rules.bonuses[bonusGoal];
			}
		}
	}
	
	return total_points;
}