module.exports = function (app, models) {
	var user_module = {};
	
	user_module.login = function (req, callback) {
		var query = {email: req.body.email.toLowerCase()};
		models.users.findOne(query, function (err, user) {
			var response = {};
			if (!user) {
				response.error = 'We can\'t find a user with that email.';
				return callback(response);
			}
			
			var compare = (req.body.password === user.password);
			if (compare) {
				// log user in
				req.session.email = user.email;
				var year = new Date(Date.now() + 1000 * 3600 * 24 * 365); // 1 year
				req.session.expires = year;
				req.session.cookie.maxAge = year;
				// send positive response
				response.email = user.email;
				response.name = user.name;
				response.error = false;
				return callback(response);
				
			} else {
				// send error
				response.error = 'That password doesn\'t work.';
				return callback(response);
			}
		});
	};
	
	user_module.checkLogin = function (req, callback) {
		var response = {};
		if (req.session.email) {
			models.users.findOne({'email': req.session.email}, function(err, user) {
				if (err || !user || !user.email) {
					response.error = true;
					return callback(response);
				}
				
				response.error = false;
				response.email = user.email;
				response.name = user.name;
				return callback(response);
			});
		} else {
			response.error = true;
			return callback(response);
		}
	};
	
	user_module.signup = function (req, callback) {
		var response = {};
		// check if email is valid
		if (!app.validator.isEmail(req.body.email)) {
			response.error = 'That\'s not a real email.';
			return callback(response);
		}
		// check if email is already registered
		models.users.findOne({email: req.body.email}, function(err, sameUser) {
			if (sameUser) {
				response.error = 'You already have an account under that email. Please login.';
				return callback(response);
			} else {
				// make a new user
				var user = new models.users({
					'name': req.body.name,
					'email': req.body.email.toLowerCase(),
					'password': req.body.password,
					'weeks': new Array(rules.weeks),
					'extraData': {}
				});
				user.save(function (err, newUser) {
					if (err) {
						response.error = "Oops. Something went wrong.";
						return callback(response);
					}
					response.error = false;
					response.email = newUser.email;
					response.name = newUser.name;
					// send the user their congratulations
					return callback(response);
				});
			}
		});
	};
	
	return user_module;
};