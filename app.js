var express = require('express');
var app = module.exports = express();
app.mongoose = require('mongoose');
app.bodyParser = require('body-parser');
app.cookieParser = require('cookie-parser');
app.expressSession = require('express-session');
app.mongoStore = require('connect-mongo')(app.expressSession);
app.validator = require('validator');
require('./www/global/rules');

var http = require('http');

var config = require('./config.js')(app, express);

var models = {};
models.users = require('./models/users')(app.mongoose);
models.messages = require('./models/messages')(app.mongoose);

// include modules
var user_module = require('./user')(app, models);
var data_module = require('./data')(app, models);
var message_module = require('./message')(app, models);

require('./routes')(app, models, user_module, data_module, message_module);

// protect angular routes
app.use(function(req, res) {
  // Use res.sendfile, as it streams instead of reading the file into memory.
  res.sendFile(__dirname + '/www/index.html');
});

http.createServer(app).listen(app.get('port'), function(){
	console.log("Express server listening on port " + app.get('port'));
});
