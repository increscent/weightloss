module.exports = function () {
	var date_format = {};
	
	date_format.getTime = function () {
		return (new Date()).getTime().toString();
	};
	
	date_format.getDate = function () {
		var date = new Date();
		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();
		return month + '-' + day + '-' + year;
	};
	
	return date_format;
};