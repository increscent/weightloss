import json
import collections
import os

version = '0.1.0'

def openFile(file):
	contents = open(file, 'r').read()
	return contents

def writeFile(file, content):
	open(file, 'w').write(content)

webroot = 'www/'
cssroot = 'styles/'
jsroot = 'app/'

css = ['global', 'bar', 'login', 'settings', 'dashboard', 'record', 'leaderboard', 'messages', 'animate']
js = ['app', 'factories', 'filters', 'directives', 'user/signup/signup', 'user/login/login', 'user/success/success', 'user/settings/settings', 'user/logout/logout', 'main/dashboard/dashboard', 'main/record/record', 'main/leaderboard/leaderboard', 'main/messages/messages', 'main/error/error', '../global/rules']

all_css = ''
for file in css:
	all_css += openFile(webroot + cssroot + file + '.css') + '\n'
writeFile(webroot + 'production-' + version + '.css', all_css)

all_js = ''
for file in js:
	all_js += openFile(webroot + jsroot + file + '.js') + '\n'
writeFile(webroot + 'production-' + version + '.js', all_js)
