wlApp

.controller('errorController', function ($scope, $timeout, errorFactory) {
	$scope.errorMessage = '';
	
	errorFactory.register( function (errorMessage) {
		$scope.errorMessage = errorMessage;
		$timeout( function () {
			$scope.errorMessage = '';
		}, 3000);
	});
});