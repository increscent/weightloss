wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/record', {
			templateUrl: '/app/main/record/record.tpl.html',
			controller: 'recordController'
		});
})

.controller('recordController', function ($scope, $location, $timeout, loginFactory, dataFactory, rulesFactory, dateFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	// initialize dates
	$scope.recordDates = [
		{
			time: new Date(),
			prettyDate: dateFactory.getPrettyDate(),
			actualDate: dateFactory.getDate(),
			week: rulesFactory.getWeek()
		},
		{
			time: new Date((new Date()).getTime() - (1000 * 60 * 60 * 24)),
			prettyDate: dateFactory.getPrettyDate((new Date()).getTime() - (1000 * 60 * 60 * 24)),
			actualDate: dateFactory.getYesterdayDate(),
			week: rulesFactory.getWeek(true)
		}
	];
	
	$scope.recordDate = false;
	
	// initialize goals
	$scope.savedGoals = {};
	
	$scope.updateData = function () {
		$scope.goals = rulesFactory.getDailyGoals($scope.recordDate.time, $scope.recordDate.week);
		dataFactory.getDayData($scope.recordDate.week, $scope.recordDate.actualDate, function (result) {
			$scope.savedGoals = result.data;
			if (!$scope.savedGoals) {
				$scope.savedGoals = {};
			}
		});
	};
	
	$scope.updateGoal = function (goal_name, goal_option) {
		if (!$scope.recordDate) {
			return;
		}
		if ($scope.savedGoals[goal_name] && $scope.savedGoals[goal_name] === goal_option) {
			delete $scope.savedGoals[goal_name];
		} else {
			$scope.savedGoals[goal_name] = goal_option;
		}
		
		if (!rulesFactory.competitionOpen()) {
			return;
		}
		
		if ($scope.saveTimeout) {
			$timeout.cancel($scope.saveTimeout);
		}
		
		$scope.loading = true;
		$scope.saveTimeout = $timeout( function () {
			$scope.saveGoals();
		}, 1000);
	};
	
	$scope.saveGoals = function () {
		if (!$scope.recordDate) {
			consoel.log('hello');
			$scope.loading = false;
			return;
		}
		dataFactory.saveDayData($scope.recordDate.week, $scope.recordDate.actualDate, $scope.savedGoals, function (result) {
			$scope.loading = false;
		});
	};
	
	$scope.location = $location;
	$scope.dateFactory = dateFactory;
	$scope.loading = false;
});