wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/leaderboard', {
			templateUrl: '/app/main/leaderboard/leaderboard.tpl.html',
			controller: 'leaderboardController'
		});
})

.controller('leaderboardController', function ($scope, $location, loginFactory, dataFactory, rulesFactory, dateFactory) {
	// get user
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
	});
	
	$scope.rankings = [];
	dataFactory.getRankingsData( function (result) {
		$scope.rankings = result.data;
	});
	
	$scope.location = $location;
});