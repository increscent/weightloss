wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/messages', {
			templateUrl: '/app/main/messages/messages.tpl.html',
			controller: 'messagesController'
		});
})

.controller('messagesController', function ($scope, $location, $timeout, loginFactory, dateFactory, messagesFactory) {
	// check for user login
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
	});
	
	messagesFactory.getAllMessages( function (result) {
		$scope.messages = result.data;
	});
	
	$scope.saveMessage = function () {
		messagesFactory.saveMessage($scope.newMessageText, function (result) {
			if (result.error) return;
			
			$scope.newMessageText = '';
			
			$scope.messages[Object.keys($scope.messages).length] = result.message;
		});
	};
	
	$scope.location = $location;
});