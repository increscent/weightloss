wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/dashboard', {
			templateUrl: '/app/main/dashboard/dashboard.tpl.html',
			controller: 'dashboardController'
		});
})

.controller('dashboardController', function ($scope, $location, loginFactory, dataFactory, rulesFactory, dateFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	// console.log(rulesFactory.hasStarted());
	
	$scope.dailyPossiblePoints = rulesFactory.possiblePoints();
	$scope.dailyActualPoints = 0;
	// get current score for the day
	dataFactory.getDayData(rulesFactory.getWeek(), dateFactory.getDate(), function (result) {
		var dailyGoals = rulesFactory.getDailyGoals(new Date(), rulesFactory.getWeek());
		var total = 0;
		for (var goal in dailyGoals) {
			if (result.data && result.data[goal]) {
				var points = dailyGoals[goal][result.data[goal]].points;
				if (dailyGoals[goal][result.data[goal]].weeklyDouble) {
					points *= 2;
				}
				total += points;
			}
		}
		$scope.dailyActualPoints = total;
	});
	
	$scope.totalPoints = 0;
	dataFactory.getPointsData( function (result) {
		$scope.totalPoints = result.data.total;
	});
	
	$scope.rank = 1;
	dataFactory.getRankingsData( function (result) {
		for (var i = 0; i < result.data.length; i++) {
			if (result.data[i].email === $scope.user.email) {
				$scope.rank = result.data[i].rank;
				return;
			}
		}
	});
	
	$scope.currentCompetitionWeek = rulesFactory.getWeek() + 1;
	$scope.totalCompetitionWeeks = rules.weeks;
	
	var endDate = dateFactory.getEndDate().getTime();
	var currentDate = (new Date()).getTime();
	$scope.daysRemaining = Math.floor((endDate - currentDate) / (1000 * 60 * 60 * 24)) + 1;
	
	$scope.location = $location;
});