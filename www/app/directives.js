wlApp

.directive('goalComplete', function ($timeout) {
	return {
		link: function ($scope, element, attributes) {
			$scope.goalName = attributes.goalName;
			$scope.$watch('savedGoals[goalName]', function () {
				if (attributes.goalComplete === 'true') {
					element.find('i').removeClass('fa-square-o').addClass('fa-check-square-o');
				} else {
					element.find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
				}
			});
		}
	};
})

.directive('spinLoading', function ($timeout) {
	return {
		link: function ($scope, element, attributes) {
			$scope.$watch('loading', function () {
				if ($scope.loading) {
					element.addClass('fa-spin');
				} else {
					element.removeClass('fa-spin');
				}
			});
		}
	};
})

.directive('autoResize', function ($timeout) {
	return {
		link: function ($scope, element, attributes) {
			var resize = function (element) {
				var sizing_element = element.next();
				var height = sizing_element[0].clientHeight;
				if (height < 20) height = 20;
				element[0].style.height = (height + 24) + 'px';
			};
			
			// init
			$timeout(function(){
				resize(element);
			});
			// on keypress
			element.bind('input', function (event) {
				resize(element);
			});
		}
	};
})

;