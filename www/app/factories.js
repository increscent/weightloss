wlApp

.factory('errorFactory', function () {
	var factory = {};
	
	factory.errorCallback = {};
	
	factory.register = function (errorCallback) {
		factory.errorCallback = errorCallback;
	};
	
	factory.errorMessage = function (message) {
		factory.errorCallback(message);
	};
	
	return factory;
})

.factory('loginFactory', function ($http) {
	var user = {};
	
	var factory = {};
	
	factory.getUser = function () {
		if (!user.email) {
			return false;
		}
		return user;
	};
	
	factory.checkLogin = function (callback) {
		http_post($http, '/check/login', {}, function (data) {
			if (data.error) {
				user = {};
				if (callback) return callback();
			}
			user = {
				'email': data.email,
				'name': data.name
			};
			
			if (callback) return callback();
		});
	};
	
	factory.login = function (email, password, callback) {
		var query = {
			'email': email,
			'password': password
		};
		
		http_post($http, '/login', query, function (data) {
			if (!data.error) {
				user = {
					'email': data.email,
					'name': data.name
				};
				return callback(user);
			} else {
				return callback(data);
			}
		});
	};
	
	factory.logout = function (callback) {
		http_post($http, '/logout', {}, function (data) {
			user = {};
			return callback();
		});
	};
	
	return factory;
})

.factory('messagesFactory', function ($http, errorFactory) {
	var factory = {};
	
	factory.getAllMessages = function (callback) {
		http_post($http, '/get/messages/all', {}, function (data) {
			if (data.error) errorFactory.errorMessage(data.error);
			
			return callback(data);
		});
	};
	
	factory.saveMessage = function (text, callback) {
		http_post($http, '/save/message', {text: text}, function (data) {
			if (data.error) errorFactory.errorMessage(data.error);
			
			return callback(data);
		});
	};
	
	return factory;
})

.factory('dataFactory', function($http) {
	var factory = {};
	
	factory.getAllData = function (callback) {
		http_post($http, '/get/data/all', {}, function (data) {
			return callback(data);
		});
	};
	
	factory.getPointsData = function (callback) {
		http_post($http, '/get/data/points', {}, function (data) {
			return callback(data);
		});
	};
	
	factory.getRankingsData = function (callback) {
		http_post($http, '/get/data/rankings', {}, function (data) {
			return callback(data);
		});
	};
	
	factory.getDayData = function (week, date, callback) {
		var query = {
			week: week,
			date: date
		};
		
		http_post($http, '/get/data/day', query, function (data) {
			return callback(data);
		});
	};
	
	factory.saveDayData = function (week, date, data, callback) {
		var query = {
			week: week,
			date: date,
			data: data
		};
		
		http_post($http, '/save/data/day', query, function (data) {
			return callback(data);
		});
	};
	
	return factory;
})

.factory('rulesFactory', function (dateFactory) {
	var factory = {};
	
	factory.competitionOpen = function () {
		var startDate = dateFactory.getStartDate();
		var endDate = dateFactory.getEndDate();
		console.log(startDate.getTime());
		console.log(endDate.getTime());
		return (startDate.getTime() <= (new Date()).getTime()) && ((new Date()).getTime() <= endDate.getTime());
	};
	
	factory.getWeek = function (yesterday) {
		var currentTime = new Date();
		if (yesterday) {
			currentTime = new Date(currentTime.getTime() - (1000 * 60 * 60 * 24));
		}
		var startTime = dateFactory.getStartDate();
		var week = Math.floor((currentTime.getTime() - startTime.getTime()) / (1000 * 60 * 60 * 24 * 7));
		return week;
	};
	
	factory.possiblePoints = function () {
		var dailyGoals = factory.getDailyGoals(new Date(), factory.getWeek());
		
		var total = 0;
		for (var goal in dailyGoals) {
			var highest = 0;
			for (var option in dailyGoals[goal]) {
				var points = dailyGoals[goal][option].points;
				if (dailyGoals[goal][option].weeklyDouble) {
					points *= 2;
				}
				if (points > highest) {
					highest = points;
				}
			}
			total += highest;
		}
		return total;
	};
	
	factory.getDailyGoals = function (currentTime, week) {
		if (!factory.competitionOpen()) return {};
		var dailyGoals = JSON.parse(JSON.stringify(rules.dailyGoals));
		
		// setup week and date
		// var currentTime = new Date();
		// var week = factory.getWeek();
		// var date = dateFactory.getDate();
		
		// handle exceptions
		for (var exception in rules.exceptions) {
			for (var i = 0; i < rules.exceptions[exception].length; i++) {
				if (exception === 'sunday') {
					if (currentTime.getDay() === 0) {
						delete dailyGoals[rules.exceptions[exception][i]];
					}
				} else if (exception === 'fastSunday') {
					if (currentTime.getDay() === 0 && currentTime.getDate() <= 7) {
						delete dailyGoals[rules.exceptions[exception][i]];
					}
				}
			}
		}
		
		// weekly double
		if (rules.weeklyDouble[week]) {
			for (var option in dailyGoals[rules.weeklyDouble[week]]) {
				dailyGoals[rules.weeklyDouble[week]][option].weeklyDouble = true;
			}
		}
		
		return dailyGoals;
	};
	
	return factory;
})

.factory('dateFactory', function () {
	var factory = {};
	
	factory.getDate = function () {
		var date = new Date();
		var year = date.getFullYear();
		var month = (date.getMonth() + 1).toString();
		if (month.length < 2)  {
			month = '0' + month;
		}
		var day = date.getDate().toString();
		if (day.length < 2)  {
			day = '0' + day;
		}
		return year + '-' + month + '-' + day;
	};
	
	factory.getYesterdayDate = function () {
		var date = new Date();
		date = new Date(date.getTime() - (1000 * 60 * 60 * 24));
		var year = date.getFullYear();
		var month = (date.getMonth() + 1).toString();
		if (month.length < 2)  {
			month = '0' + month;
		}
		var day = date.getDate().toString();
		if (day.length < 2)  {
			day = '0' + day;
		}
		return year + '-' + month + '-' + day;
	};
	
	factory.getPrettyDate = function (date) {
		var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		if (date) {
			date = new Date(date);
		} else {
			date = new Date();
		}
		var day = date.getDate();
		var month = monthNames[date.getMonth()];
		return month + ' ' + day;
	};
	
	factory.getStartDate = function () {
		var startDate = new Date();
		startDate.setDate(rules.startDate);
		startDate.setMonth(rules.startMonth);
		startDate.setFullYear(rules.startYear);
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);
		startDate.setMilliseconds(0);
		return startDate;
	};
	
	factory.getEndDate = function () {
		var startDate = factory.getStartDate();
		var endDate = new Date(startDate.getTime() + 1000 * 60 * 60 * 24 * 7 * rules.weeks);
		return endDate;
	};
	
	return factory;
});


// local helper functions

function http_post($http, url, query, callback) {
	$http.post(url, query)
		.success ( function (data, status, headers, config) {
			return callback(data);
		})
		.error ( function (data, status, headers, config) {
			var result = {};
			result.error = 'Oops. Something went wrong with your request.';
			return callback(result);
		});
}