wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/signup', {
			templateUrl: '/app/user/signup/signup.tpl.html',
			controller: 'signupController'
		});
})

.controller('signupController', function($scope, $location, $http, $window, loginFactory) {
	// make sure user is not logged in already
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if ($scope.user) {
			location.path('/dashboard');
		}
	});
	
	$scope.errorMessage = false;
	$scope.location = $location;
	
	// show placeholder in smaller screens
	$scope.width = $window.innerWidth;
	if ($scope.width < 768) {
		$scope.placeholder = {};
		$scope.placeholder.name = 'name';
		$scope.placeholder.email = 'email';
		$scope.placeholder.password = 'password';
		$scope.placeholder.again = '(again)';
	}
	
	$scope.signup = function () {
		// check if passwords match
		if ($scope.password1 !== $scope.password2) {
			$scope.errorMessage = "Those passwords don't match.";
			return;
		}
		
		$scope.errorMessage = false;
		
		var user = {
			'name': $scope.name,
			'email': $scope.email,
			'password': $scope.password1
		};
		
		$http.post('/signup', user).
			success( function (data, status, headers, config) {
				if (!data.error) {
					loginFactory.login(user.email, user.password, function (result) {
						if (result.error) {
							$scope.errorMessage = result.error;
							$scope.$apply();
						} else {
							$location.path('/success');
						}
					});
				} else {
					$scope.errorMessage = data.error;
					$scope.$apply();
				}
			});
	};
});