wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/settings', {
			templateUrl: '/app/user/settings/settings.tpl.html',
			controller: 'settingsController'
		});
})

.controller('settingsController', function ($scope, $location, loginFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	$scope.location = $location;
});