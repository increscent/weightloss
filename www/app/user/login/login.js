wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/login', {
			templateUrl: '/app/user/login/login.tpl.html',
			controller: 'loginController'
		});
})

.controller('loginController', function($scope, $location, $window, loginFactory) {
	// make sure user is not logged in already
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if ($scope.user) {
			$location.path('/dashboard');
		}
	});
	
	$scope.errorMessage = false;
	$scope.location = $location;
	
	// show placeholder in smaller screens
	$scope.width = $window.innerWidth;
	if ($scope.width < 768) {
		$scope.placeholder = {};
		$scope.placeholder.email = 'email';
		$scope.placeholder.password = 'password';
	}
	
	$scope.login = function () {
		$scope.errorMessage = false;
			
		loginFactory.login($scope.email, $scope.password, function (result) {
			if (result.error) {
				$scope.errorMessage = result.error;
				return;
			}
			
			$location.path('/dashboard');
		});
	};
});