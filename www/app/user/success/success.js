wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/success', {
			templateUrl: '/app/user/success/success.tpl.html',
			controller: 'successController'
		});
})

.controller('successController', function($scope, $location, loginFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	$scope.location = $location;
});