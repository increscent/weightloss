wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/logout', {
			templateUrl: '/app/user/logout/logout.tpl.html',
			controller: 'logoutController'
		});
})

.controller('logoutController', function($scope, $location, loginFactory) {
	loginFactory.logout(function () {
		$location.path('/login');
		$scope.$apply();
	});
});