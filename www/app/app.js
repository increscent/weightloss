var wlApp = angular.module('wlApp', ['ngRoute', 'ngTouch', 'ngAnimate']);

wlApp.config(function ($routeProvider, $locationProvider) {
	$routeProvider
		.otherwise({redirectTo: '/login'});
		
	$locationProvider.html5Mode(true);
});