var rules = {};

rules.startDate = '13';
rules.startMonth = '6';
rules.startYear = '2017';

rules.weeks = 8;

rules.dailyGoals = {
	exercise: {
		1: {
			name: '30 minutes of exercise',
			points: 5
		},
		2: {
			name: '45 minutes of exercise',
			points: 7
		}
	},
	vegetables: {
		1: {
			name: 'Eat 4 servings of vegetables',
			points: 5
		}
	},
	sugar: {
		1: {
			name: 'No eating sugar',
			points: 5
		}
	},
	pm: {
		1: {
			name: 'No eating past 9:00 PM',
			points: 3
		}
	},
	water: {
		1: {
			name: 'Drink 64oz of water',
			points: 3
		}
	}
};

rules.exceptions = {
	sunday: ['exercise'],
	fastSunday: ['water']
};

rules.bonuses = {
	sugar: 5
};

rules.weeklyDouble = [
	'',
	'vegetables',
	'sugar',
	'water',
	'pm',
	'exercise',
	'vegetables',
	'sugar'
];

if (typeof global !== 'undefined') {
	global.rules = rules;
}
