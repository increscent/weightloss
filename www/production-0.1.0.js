var wlApp = angular.module('wlApp', ['ngRoute', 'ngTouch', 'ngAnimate']);

wlApp.config(function ($routeProvider, $locationProvider) {
	$routeProvider
		.otherwise({redirectTo: '/login'});
		
	$locationProvider.html5Mode(true);
});
wlApp

.factory('errorFactory', function () {
	var factory = {};
	
	factory.errorCallback = {};
	
	factory.register = function (errorCallback) {
		factory.errorCallback = errorCallback;
	};
	
	factory.errorMessage = function (message) {
		factory.errorCallback(message);
	};
	
	return factory;
})

.factory('loginFactory', function ($http) {
	var user = {};
	
	var factory = {};
	
	factory.getUser = function () {
		if (!user.email) {
			return false;
		}
		return user;
	};
	
	factory.checkLogin = function (callback) {
		http_post($http, '/check/login', {}, function (data) {
			if (data.error) {
				user = {};
				if (callback) return callback();
			}
			user = {
				'email': data.email,
				'name': data.name
			};
			
			if (callback) return callback();
		});
	};
	
	factory.login = function (email, password, callback) {
		var query = {
			'email': email,
			'password': password
		};
		
		http_post($http, '/login', query, function (data) {
			if (!data.error) {
				user = {
					'email': data.email,
					'name': data.name
				};
				return callback(user);
			} else {
				return callback(data);
			}
		});
	};
	
	factory.logout = function (callback) {
		http_post($http, '/logout', {}, function (data) {
			user = {};
			return callback();
		});
	};
	
	return factory;
})

.factory('messagesFactory', function ($http, errorFactory) {
	var factory = {};
	
	factory.getAllMessages = function (callback) {
		http_post($http, '/get/messages/all', {}, function (data) {
			if (data.error) errorFactory.errorMessage(data.error);
			
			return callback(data);
		});
	};
	
	factory.saveMessage = function (text, callback) {
		http_post($http, '/save/message', {text: text}, function (data) {
			if (data.error) errorFactory.errorMessage(data.error);
			
			return callback(data);
		});
	};
	
	return factory;
})

.factory('dataFactory', function($http) {
	var factory = {};
	
	factory.getAllData = function (callback) {
		http_post($http, '/get/data/all', {}, function (data) {
			return callback(data);
		});
	};
	
	factory.getPointsData = function (callback) {
		http_post($http, '/get/data/points', {}, function (data) {
			return callback(data);
		});
	};
	
	factory.getRankingsData = function (callback) {
		http_post($http, '/get/data/rankings', {}, function (data) {
			return callback(data);
		});
	};
	
	factory.getDayData = function (week, date, callback) {
		var query = {
			week: week,
			date: date
		};
		
		http_post($http, '/get/data/day', query, function (data) {
			return callback(data);
		});
	};
	
	factory.saveDayData = function (week, date, data, callback) {
		var query = {
			week: week,
			date: date,
			data: data
		};
		
		http_post($http, '/save/data/day', query, function (data) {
			return callback(data);
		});
	};
	
	return factory;
})

.factory('rulesFactory', function (dateFactory) {
	var factory = {};
	
	factory.competitionOpen = function () {
		var startDate = dateFactory.getStartDate();
		var endDate = dateFactory.getEndDate();
		console.log(startDate.getTime());
		console.log(endDate.getTime());
		return (startDate.getTime() <= (new Date()).getTime()) && ((new Date()).getTime() <= endDate.getTime());
	};
	
	factory.getWeek = function (yesterday) {
		var currentTime = new Date();
		if (yesterday) {
			currentTime = new Date(currentTime.getTime() - (1000 * 60 * 60 * 24));
		}
		var startTime = dateFactory.getStartDate();
		var week = Math.floor((currentTime.getTime() - startTime.getTime()) / (1000 * 60 * 60 * 24 * 7));
		return week;
	};
	
	factory.possiblePoints = function () {
		var dailyGoals = factory.getDailyGoals(new Date(), factory.getWeek());
		
		var total = 0;
		for (var goal in dailyGoals) {
			var highest = 0;
			for (var option in dailyGoals[goal]) {
				var points = dailyGoals[goal][option].points;
				if (dailyGoals[goal][option].weeklyDouble) {
					points *= 2;
				}
				if (points > highest) {
					highest = points;
				}
			}
			total += highest;
		}
		return total;
	};
	
	factory.getDailyGoals = function (currentTime, week) {
		if (!factory.competitionOpen()) return {};
		var dailyGoals = JSON.parse(JSON.stringify(rules.dailyGoals));
		
		// setup week and date
		// var currentTime = new Date();
		// var week = factory.getWeek();
		// var date = dateFactory.getDate();
		
		// handle exceptions
		for (var exception in rules.exceptions) {
			for (var i = 0; i < rules.exceptions[exception].length; i++) {
				if (exception === 'sunday') {
					if (currentTime.getDay() === 0) {
						delete dailyGoals[rules.exceptions[exception][i]];
					}
				} else if (exception === 'fastSunday') {
					if (currentTime.getDay() === 0 && currentTime.getDate() <= 7) {
						delete dailyGoals[rules.exceptions[exception][i]];
					}
				}
			}
		}
		
		// weekly double
		if (rules.weeklyDouble[week]) {
			for (var option in dailyGoals[rules.weeklyDouble[week]]) {
				dailyGoals[rules.weeklyDouble[week]][option].weeklyDouble = true;
			}
		}
		
		return dailyGoals;
	};
	
	return factory;
})

.factory('dateFactory', function () {
	var factory = {};
	
	factory.getDate = function () {
		var date = new Date();
		var year = date.getFullYear();
		var month = (date.getMonth() + 1).toString();
		if (month.length < 2)  {
			month = '0' + month;
		}
		var day = date.getDate().toString();
		if (day.length < 2)  {
			day = '0' + day;
		}
		return year + '-' + month + '-' + day;
	};
	
	factory.getYesterdayDate = function () {
		var date = new Date();
		date = new Date(date.getTime() - (1000 * 60 * 60 * 24));
		var year = date.getFullYear();
		var month = (date.getMonth() + 1).toString();
		if (month.length < 2)  {
			month = '0' + month;
		}
		var day = date.getDate().toString();
		if (day.length < 2)  {
			day = '0' + day;
		}
		return year + '-' + month + '-' + day;
	};
	
	factory.getPrettyDate = function (date) {
		var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		if (date) {
			date = new Date(date);
		} else {
			date = new Date();
		}
		var day = date.getDate();
		var month = monthNames[date.getMonth()];
		return month + ' ' + day;
	};
	
	factory.getStartDate = function () {
		var startDate = new Date();
		startDate.setDate(rules.startDate);
		startDate.setMonth(rules.startMonth);
		startDate.setFullYear(rules.startYear);
		startDate.setHours(0);
		startDate.setMinutes(0);
		startDate.setSeconds(0);
		startDate.setMilliseconds(0);
		return startDate;
	};
	
	factory.getEndDate = function () {
		var startDate = factory.getStartDate();
		var endDate = new Date(startDate.getTime() + 1000 * 60 * 60 * 24 * 7 * rules.weeks);
		return endDate;
	};
	
	return factory;
});


// local helper functions

function http_post($http, url, query, callback) {
	$http.post(url, query)
		.success ( function (data, status, headers, config) {
			return callback(data);
		})
		.error ( function (data, status, headers, config) {
			var result = {};
			result.error = 'Oops. Something went wrong with your request.';
			return callback(result);
		});
}
wlApp

.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
		var filtered = [];
		angular.forEach(items, function(item) {
			filtered.push(item);
		});
		if (field) {
			filtered.sort(function (a, b) {
				return (a[field] > b[field] ? 1 : -1);
			});
		}
		if(reverse) filtered.reverse();
		return filtered;
	};
})

;
wlApp

.directive('goalComplete', function ($timeout) {
	return {
		link: function ($scope, element, attributes) {
			$scope.goalName = attributes.goalName;
			$scope.$watch('savedGoals[goalName]', function () {
				if (attributes.goalComplete === 'true') {
					element.find('i').removeClass('fa-square-o').addClass('fa-check-square-o');
				} else {
					element.find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
				}
			});
		}
	};
})

.directive('spinLoading', function ($timeout) {
	return {
		link: function ($scope, element, attributes) {
			$scope.$watch('loading', function () {
				if ($scope.loading) {
					element.addClass('fa-spin');
				} else {
					element.removeClass('fa-spin');
				}
			});
		}
	};
})

.directive('autoResize', function ($timeout) {
	return {
		link: function ($scope, element, attributes) {
			var resize = function (element) {
				var sizing_element = element.next();
				var height = sizing_element[0].clientHeight;
				if (height < 20) height = 20;
				element[0].style.height = (height + 24) + 'px';
			};
			
			// init
			$timeout(function(){
				resize(element);
			});
			// on keypress
			element.bind('input', function (event) {
				resize(element);
			});
		}
	};
})

;
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/signup', {
			templateUrl: '/app/user/signup/signup.tpl.html',
			controller: 'signupController'
		});
})

.controller('signupController', function($scope, $location, $http, $window, loginFactory) {
	// make sure user is not logged in already
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if ($scope.user) {
			location.path('/dashboard');
		}
	});
	
	$scope.errorMessage = false;
	$scope.location = $location;
	
	// show placeholder in smaller screens
	$scope.width = $window.innerWidth;
	if ($scope.width < 768) {
		$scope.placeholder = {};
		$scope.placeholder.name = 'name';
		$scope.placeholder.email = 'email';
		$scope.placeholder.password = 'password';
		$scope.placeholder.again = '(again)';
	}
	
	$scope.signup = function () {
		// check if passwords match
		if ($scope.password1 !== $scope.password2) {
			$scope.errorMessage = "Those passwords don't match.";
			return;
		}
		
		$scope.errorMessage = false;
		
		var user = {
			'name': $scope.name,
			'email': $scope.email,
			'password': $scope.password1
		};
		
		$http.post('/signup', user).
			success( function (data, status, headers, config) {
				if (!data.error) {
					loginFactory.login(user.email, user.password, function (result) {
						if (result.error) {
							$scope.errorMessage = result.error;
							$scope.$apply();
						} else {
							$location.path('/success');
						}
					});
				} else {
					$scope.errorMessage = data.error;
					$scope.$apply();
				}
			});
	};
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/login', {
			templateUrl: '/app/user/login/login.tpl.html',
			controller: 'loginController'
		});
})

.controller('loginController', function($scope, $location, $window, loginFactory) {
	// make sure user is not logged in already
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if ($scope.user) {
			$location.path('/dashboard');
		}
	});
	
	$scope.errorMessage = false;
	$scope.location = $location;
	
	// show placeholder in smaller screens
	$scope.width = $window.innerWidth;
	if ($scope.width < 768) {
		$scope.placeholder = {};
		$scope.placeholder.email = 'email';
		$scope.placeholder.password = 'password';
	}
	
	$scope.login = function () {
		$scope.errorMessage = false;
			
		loginFactory.login($scope.email, $scope.password, function (result) {
			if (result.error) {
				$scope.errorMessage = result.error;
				return;
			}
			
			$location.path('/dashboard');
		});
	};
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/success', {
			templateUrl: '/app/user/success/success.tpl.html',
			controller: 'successController'
		});
})

.controller('successController', function($scope, $location, loginFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	$scope.location = $location;
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/settings', {
			templateUrl: '/app/user/settings/settings.tpl.html',
			controller: 'settingsController'
		});
})

.controller('settingsController', function ($scope, $location, loginFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	$scope.location = $location;
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/logout', {
			templateUrl: '/app/user/logout/logout.tpl.html',
			controller: 'logoutController'
		});
})

.controller('logoutController', function($scope, $location, loginFactory) {
	loginFactory.logout(function () {
		$location.path('/login');
		$scope.$apply();
	});
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/dashboard', {
			templateUrl: '/app/main/dashboard/dashboard.tpl.html',
			controller: 'dashboardController'
		});
})

.controller('dashboardController', function ($scope, $location, loginFactory, dataFactory, rulesFactory, dateFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	// console.log(rulesFactory.hasStarted());
	
	$scope.dailyPossiblePoints = rulesFactory.possiblePoints();
	$scope.dailyActualPoints = 0;
	// get current score for the day
	dataFactory.getDayData(rulesFactory.getWeek(), dateFactory.getDate(), function (result) {
		var dailyGoals = rulesFactory.getDailyGoals(new Date(), rulesFactory.getWeek());
		var total = 0;
		for (var goal in dailyGoals) {
			if (result.data && result.data[goal]) {
				var points = dailyGoals[goal][result.data[goal]].points;
				if (dailyGoals[goal][result.data[goal]].weeklyDouble) {
					points *= 2;
				}
				total += points;
			}
		}
		$scope.dailyActualPoints = total;
	});
	
	$scope.totalPoints = 0;
	dataFactory.getPointsData( function (result) {
		$scope.totalPoints = result.data.total;
	});
	
	$scope.rank = 1;
	dataFactory.getRankingsData( function (result) {
		for (var i = 0; i < result.data.length; i++) {
			if (result.data[i].email === $scope.user.email) {
				$scope.rank = result.data[i].rank;
				return;
			}
		}
	});
	
	$scope.currentCompetitionWeek = rulesFactory.getWeek() + 1;
	$scope.totalCompetitionWeeks = rules.weeks;
	
	var endDate = dateFactory.getEndDate().getTime();
	var currentDate = (new Date()).getTime();
	$scope.daysRemaining = Math.floor((endDate - currentDate) / (1000 * 60 * 60 * 24)) + 1;
	
	$scope.location = $location;
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/record', {
			templateUrl: '/app/main/record/record.tpl.html',
			controller: 'recordController'
		});
})

.controller('recordController', function ($scope, $location, $timeout, loginFactory, dataFactory, rulesFactory, dateFactory) {
	// make sure user is logged in
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
		if (!$scope.user) {
			$location.path('/login');
		}
	});
	
	// initialize dates
	$scope.recordDates = [
		{
			time: new Date(),
			prettyDate: dateFactory.getPrettyDate(),
			actualDate: dateFactory.getDate(),
			week: rulesFactory.getWeek()
		},
		{
			time: new Date((new Date()).getTime() - (1000 * 60 * 60 * 24)),
			prettyDate: dateFactory.getPrettyDate((new Date()).getTime() - (1000 * 60 * 60 * 24)),
			actualDate: dateFactory.getYesterdayDate(),
			week: rulesFactory.getWeek(true)
		}
	];
	
	$scope.recordDate = false;
	
	// initialize goals
	$scope.savedGoals = {};
	
	$scope.updateData = function () {
		$scope.goals = rulesFactory.getDailyGoals($scope.recordDate.time, $scope.recordDate.week);
		dataFactory.getDayData($scope.recordDate.week, $scope.recordDate.actualDate, function (result) {
			$scope.savedGoals = result.data;
			if (!$scope.savedGoals) {
				$scope.savedGoals = {};
			}
		});
	};
	
	$scope.updateGoal = function (goal_name, goal_option) {
		if (!$scope.recordDate) {
			return;
		}
		if ($scope.savedGoals[goal_name] && $scope.savedGoals[goal_name] === goal_option) {
			delete $scope.savedGoals[goal_name];
		} else {
			$scope.savedGoals[goal_name] = goal_option;
		}
		
		if (!rulesFactory.competitionOpen()) {
			return;
		}
		
		if ($scope.saveTimeout) {
			$timeout.cancel($scope.saveTimeout);
		}
		
		$scope.loading = true;
		$scope.saveTimeout = $timeout( function () {
			$scope.saveGoals();
		}, 1000);
	};
	
	$scope.saveGoals = function () {
		if (!$scope.recordDate) {
			consoel.log('hello');
			$scope.loading = false;
			return;
		}
		dataFactory.saveDayData($scope.recordDate.week, $scope.recordDate.actualDate, $scope.savedGoals, function (result) {
			$scope.loading = false;
		});
	};
	
	$scope.location = $location;
	$scope.dateFactory = dateFactory;
	$scope.loading = false;
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/leaderboard', {
			templateUrl: '/app/main/leaderboard/leaderboard.tpl.html',
			controller: 'leaderboardController'
		});
})

.controller('leaderboardController', function ($scope, $location, loginFactory, dataFactory, rulesFactory, dateFactory) {
	// get user
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
	});
	
	$scope.rankings = [];
	dataFactory.getRankingsData( function (result) {
		$scope.rankings = result.data;
	});
	
	$scope.location = $location;
});
wlApp

.config( function ($routeProvider) {
	$routeProvider
		.when('/messages', {
			templateUrl: '/app/main/messages/messages.tpl.html',
			controller: 'messagesController'
		});
})

.controller('messagesController', function ($scope, $location, $timeout, loginFactory, dateFactory, messagesFactory) {
	// check for user login
	loginFactory.checkLogin(function () {
		$scope.user = loginFactory.getUser();
	});
	
	messagesFactory.getAllMessages( function (result) {
		$scope.messages = result.data;
	});
	
	$scope.saveMessage = function () {
		messagesFactory.saveMessage($scope.newMessageText, function (result) {
			if (result.error) return;
			
			$scope.newMessageText = '';
			
			$scope.messages[Object.keys($scope.messages).length] = result.message;
		});
	};
	
	$scope.location = $location;
});
wlApp

.controller('errorController', function ($scope, $timeout, errorFactory) {
	$scope.errorMessage = '';
	
	errorFactory.register( function (errorMessage) {
		$scope.errorMessage = errorMessage;
		$timeout( function () {
			$scope.errorMessage = '';
		}, 3000);
	});
});
var rules = {};

rules.startDate = '26';
rules.startMonth = '7';
rules.startYear = '2014';

rules.weeks = 8;

rules.dailyGoals = {
	exercise: {
		1: {
			name: '30 minutes of exercise',
			points: 5
		},
		2: {
			name: '45 minutes of exercise',
			points: 7
		}
	},
	vegetables: {
		1: {
			name: 'Eat 4 servings of vegetables',
			points: 5
		}
	},
	sugar: {
		1: {
			name: 'No eating sugar',
			points: 5
		}
	},
	pm: {
		1: {
			name: 'No eating past 9:00 PM',
			points: 3
		}
	},
	water: {
		1: {
			name: 'Drink 64oz of water',
			points: 3
		}
	}
};

rules.exceptions = {
	sunday: ['exercise'],
	fastSunday: ['water']
};

rules.bonuses = {
	sugar: 5
};

rules.weeklyDouble = [
	'',
	'vegetables',
	'sugar',
	'water',
	'pm',
	'exercise',
	'vegetables',
	'sugar'
];

if (typeof global !== 'undefined') {
	global.rules = rules;
}

