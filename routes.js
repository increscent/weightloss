module.exports = function(app, models, user_module, data_module, message_module){
	
	app.get('/testsession', function (req, res) {
		res.send(req.session.email);
	});

	app.post('/signup', function (req, res){
		user_module.signup(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/login', function (req, res) {
		user_module.login(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/check/login', function (req, res) {
		user_module.checkLogin(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/logout', function (req, res) {
		req.session.regenerate(function (err) {
			res.send({error: false});
		});
	});
	
	app.post('/get/data/all', function (req, res) {
		data_module.getAllData(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/get/data/points', function (req, res) {
		data_module.getPointsData(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/get/data/rankings', function (req, res) {
		data_module.getRankingsData(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/get/data/day', function (req, res) {
		data_module.getDayData(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/save/data/day', function (req, res) {
		data_module.saveDayData(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/get/messages/all', function (req, res) {
		message_module.getAllMessages(req, function (result) {
			res.send(result);
		});
	});
	
	app.post('/save/message', function (req, res) {
		message_module.saveMessage(req, function (result) {
			res.send(result);
		});
	});
	
	// app.post('/new/card', function (req, res) {
	// 	notes_module.newCard(req, function (result) {
	// 		res.send(result);
	// 	});
	// });
	
	// app.post('/save/card', function (req, res) {
	// 	notes_module.saveCard(req, function (result) {
	// 		res.send(result);
	// 	});
	// });
	
	// app.post('/get/notes', function (req, res) {
	// 	notes_module.getNotes(req, function (result) {
	// 		res.send(result);
	// 	});
	// });
	
	// app.post('/new/note', function (req, res) {
	// 	notes_module.newNote(req, function (result) {
	// 		res.send(result);
	// 	});
	// });
	
	// app.post('/save/note', function (req, res) {
	// 	notes_module.saveNote(req, function (result) {
	// 		res.send(result);
	// 	});
	// });
};