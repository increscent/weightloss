module.exports = function(mongoose) {
	var Schema = mongoose.Schema;
	var usersSchema = new Schema({
		name: String,
		email: String,
		password: String,
		weeks: [],
		extraData: {}
	});
	
	return mongoose.model('users', usersSchema);
};