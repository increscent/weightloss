module.exports = function(mongoose) {
	var Schema = mongoose.Schema;
	var messagesSchema = new Schema({
		email: String,
		name: String,
		timestamp: Number,
		text: String
	});
	
	return mongoose.model('messages', messagesSchema);
};