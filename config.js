module.exports = function(app, express, mongoose){

	var config = this;

	var path = require('path');

	app.mongoose.connect('mongodb://localhost/weightloss', {useMongoClient: true});
	app.use(app.cookieParser());
	app.use(app.expressSession({
		'secret': 'F,;B?5tQ7cTr5n5a9;sSNou+dr$&,_C,',
		'saveUninitialized': true,
		'resave': true,
		'cookie': {
			// 'secure': true, //https only
			'path': '/'
		},
		store: new app.mongoStore({mongooseConnection: app.mongoose.connection})
	}));
	app.use(app.bodyParser.urlencoded({extended: true}));
	app.use(app.bodyParser.json());
	app.set('port', process.env.PORT || 6789);
	app.use(express.static(path.join(__dirname, 'www')));

	return config;
};
